﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public interface ILog
    {
        void Log(object message);
    }
}