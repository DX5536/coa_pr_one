﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class Entity : MonoBehaviour, ILog
    {
        [SerializeField]
        private string entityName;

        public string EntityName
        {
            get { return entityName; }
            set { entityName = value; }
        }

        public void Log(object message)
        {
            Debug.Log(message);
        }
    }
}