﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public interface IDamageable
    {
        void Damage(int damageAmount);
    }
}
