﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class EntityManager : MonoBehaviour, ILog
    {
        [SerializeField]
        private Entity[] entities;

        public Entity[] Entities
        {
            get { return entities; }
            set { entities = value; }
        }

        void Start()
        {
            Log("Your name is registering... ") ;
            for (int i = 0; i < entities.Length; i++)
            {
                Log("You are currently at " + i + " with the entity: " + Entities[i].EntityName);
            }
        }

        public void Log(object message)
        {
            Debug.Log(message);
        }
    }
}