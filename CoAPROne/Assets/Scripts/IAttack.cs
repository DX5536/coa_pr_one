﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public interface IAttack
    {
        void AttackEnemy(Enemy selectedEnemy, int attackAmount);
    }
}