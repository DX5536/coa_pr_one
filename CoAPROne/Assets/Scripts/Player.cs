﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attack;
        [SerializeField]
        private bool isAlive;
        [SerializeField]
        private Enemy enemyEntity;

        public int PlayerCurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        public int PlayerAttack
        {
            get { return attack; }
            set { attack = value; }
        }

        public bool PlayerIsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public Enemy PlayerEnemyEntity
        {
            get { return enemyEntity; }
            set { enemyEntity = value; }
        }


        public void AttackEnemy(Enemy selectedEnemy, int attackAmount)
        {
            if (selectedEnemy.EnemyIsAlive == true)
            {
                selectedEnemy.Damage(attackAmount);
            }
        }

        void Start()
        {
            Log("Player name is:" + EntityName);
            Log("Current Health is:" + currentHealth);
            Log("Attack power is:" + attack);

            AttackEnemy(enemyEntity, attack);
        }
    }
}