﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class Enemy : Entity, IDamageable
    {
        [SerializeField]
        private int currentHealth;
        [SerializeField]
        private int attack;
        [SerializeField]
        private bool isAlive;

        public int EnemyCurrentHealth
        {
            get { return currentHealth; }
            set { currentHealth = value; }
        }

        public int EnemyAttack
        {
            get { return attack; }
            set { attack = value; }
        }

        public bool EnemyIsAlive
        {
            get { return isAlive; }
            set { isAlive = value; }
        }

        public void Damage(int damageAmount)
        {
            if (currentHealth > 0)
            {
                if (currentHealth == 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log(EntityName + " has" + currentHealth + " HP left.");
                }
                currentHealth -= attack;
            }

            else if (currentHealth <= 0)
            {
                currentHealth = 0;
                isAlive = false;
                Log(EntityName + " has" + currentHealth + " HP left.");
            }

            Log(EntityName + " has been damaged and has " + currentHealth + " HP now.");
        }
    }
}